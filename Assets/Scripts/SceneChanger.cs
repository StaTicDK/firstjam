﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneChanger : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("LevelSelect");
    }

    public void SettingsGUI()
    {
        SceneManager.LoadScene("Settings");
    }

    public void QuitApp()
    {
        Application.Quit();
    }
}
