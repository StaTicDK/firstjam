﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb2d;
    public float speed = 10;
    public float jumpheight = 10f;
    private float horizontal;
    private bool isJumping = false;
    public laser laser;
    public laserBall laserball;
    private bool level1Ready;
    private bool level2Ready;
    private bool level3Ready;
    public Canvas gameover;
    public Canvas win;
    public Canvas popup;
    private bool hasWin = false;
    private bool isBlueState = false;
    // Start is called before the first frame update
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        Jump();
        if(Input.GetKeyDown(KeyCode.X))
        {
            if (isBlueState)
            {
                laser.freeze();
                laserball.Speedup();
            }
            else
            {
                laser.unfreeze();
                laserball.Speedown();
            }
            isBlueState = !isBlueState;
        }

        if(Input.GetKeyDown(KeyCode.Return))
        {
            if(level1Ready)
            {
                SceneManager.LoadScene("Level1");
            }

            if (level2Ready)
            {
                SceneManager.LoadScene("Level2");
            }

            if (level3Ready)
            {
                SceneManager.LoadScene("Level3");
            }
        }

        if (hasWin == true)
        {
            StartCoroutine(Wait());
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            popup.gameObject.SetActive(true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "levelDoor1")
        {
            level1Ready = true;
        }

        if (collision.gameObject.name == "levelDoor2")
        {
            level2Ready = true;
        }

        if (collision.gameObject.name == "levelDoor3")
        {
            level3Ready = true;
        }

        if (collision.gameObject.name == "GJGoal")
        {
            win.gameObject.SetActive(true);
            hasWin = true;
        }
    }

    private void FixedUpdate()
    { 

        rb2d.velocity = new Vector2(horizontal * speed, rb2d.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isJumping = false;
        rb2d.velocity = new Vector2(horizontal * speed, 0);

        if(collision.gameObject.name == "GJLaser(Clone)" || collision.gameObject.name == "GJLaserBall(Clone)" || collision.gameObject.name == "death")
        {
            gameover.gameObject.SetActive(true);
            Destroy(gameObject);
        }

        
    }

    void Jump()
    {
        if(Input.GetKeyDown(KeyCode.Space) && isJumping == false)
        {
            rb2d.AddForce(new Vector2(0f, 1f * jumpheight), ForceMode2D.Impulse);
            isJumping = true;
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("LevelSelect");
    }
}
