﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class restartGame : MonoBehaviour
{
    public GameObject Player;
    public string CurrentScene;
    public string levelselectScene;


    // Update is called once per frame
    void Update()
    {
        if (Player == null)
        {
            StartCoroutine(Wait());
        }


    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(CurrentScene);
    }
}
