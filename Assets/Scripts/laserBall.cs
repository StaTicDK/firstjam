﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserBall : MonoBehaviour
{
    private float seconds = 2.0f;
    public projectile p;
    public float speed = 20f;
    public Vector3 position;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitForNextProjectile());
    }

    IEnumerator WaitForNextProjectile()
    {
        projectile pa = Instantiate(p, transform.position + position, transform.rotation);
        pa.GetComponent<Rigidbody2D>().velocity = transform.up * speed;
        yield return new WaitForSeconds(seconds);
        StartCoroutine(WaitForNextProjectile());
    }

    public void Speedup()
    {
        seconds = 0.2f;
        speed = 5 * speed;
    }

    public void Speedown()
    {
        seconds = 2.0f;
        speed = speed / 5;
    }
}
