﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laser : MonoBehaviour
{
    private float seconds = 0.2f;
    public projectile p;
    public float speed;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitForNextProjectile());
    }

    IEnumerator WaitForNextProjectile()
    {
        projectile pa = Instantiate(p, transform.position + offset, transform.rotation);
        pa.GetComponent<Rigidbody2D>().velocity = transform.up * speed;
        yield return new WaitForSeconds(seconds);
        StartCoroutine(WaitForNextProjectile());
    }


    public void freeze()
    {
        seconds = 2.0f;
        speed = speed / 5;
    }

    public void unfreeze()
    {
        seconds = 0.2f;
        speed = speed * 5;
    }
}
